package com.classpath.io.orders.model;

import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.FetchType.LAZY;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PastOrPresent;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
@EqualsAndHashCode

//Hibernate annotations
@Entity
@Table(name="orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotEmpty(message="customer name cannot be empty")
	private String customerName;
	
	@Email(message="invalid email address")
	private String customerEmail;
	
	@Min(value = 2000, message = "order price should be more than 2000")
	@Max(value = 20000, message = "order price should be less than 20000")
	private double price;
	
	@PastOrPresent(message="order date cannot be in the future")
	private LocalDate orderDate;
	
	@OneToMany(mappedBy = "order", cascade = ALL, fetch = LAZY)
	@JsonManagedReference
	private Set<LineItem> lineItems;

	//scaffolding code
	// bridge the order and line items
	public void addLineItem(LineItem lineItem) {
		if(this.lineItems == null) {
			lineItems = new HashSet<>();
		}
		this.lineItems.add(lineItem);
		lineItem.setOrder(this);
	}
}
