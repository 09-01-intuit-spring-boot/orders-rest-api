package com.classpath.io.orders.model;

import java.time.LocalDate;

public interface OrderDto {

	String getCustomerName();

	String getCustomerEmail();
	
	double getPrice();
	
	LocalDate getOrderDate();
}
