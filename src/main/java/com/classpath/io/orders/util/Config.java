package com.classpath.io.orders.util;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

@Component
public class Config {
	
	@ConditionalOnProperty(name = "loadUser", prefix = "app", havingValue = "true", matchIfMissing = false)
	@Bean
	public User user() {
		return new User();
	}
	
	@ConditionalOnClass(name = "com.xmy.ngx")
	@Bean
	public User userBasedOnClass() {
		return new User();
	}
	
	@ConditionalOnClass(name = "com.classpath.io.orders.util.ApplicationConfig")
	@Bean
	public User userOnClass() {
		return new User();
	}
	
	
	@ConditionalOnMissingClass("com.xmy.ngx")
	@Bean
	public User userBasedOnMissingClass() {
		return new User();
	}
	
	@ConditionalOnBean(name = "user")
	@Bean
	public User userBasedBean() {
		return new User();
	}
	
	@ConditionalOnBean(name = "userBasedOnClass")
	@Bean
	public User userBasedMissingBean() {
		return new User();
	}
	
	@Bean
	public Faker faker() {
		return new Faker();
	}
}

class User {
	
}
