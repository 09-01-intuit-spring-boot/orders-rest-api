package com.classpath.io.orders.util;

import static java.util.stream.Stream.of;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.classpath.io.orders.service.OrderService;

@Component
public class ApplicationConfig implements CommandLineRunner {

	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private OrderService orderService;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("This bean is initialized");
		String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
		// imperative style

		/*
		 * for (String beanName : beanDefinitionNames) { if (beanName.contains("user"))
		 * { System.out.println(beanName); } }
		 */
		// declarative style
		of(beanDefinitionNames)
		  .filter(bean -> bean.startsWith("user"))
		  .forEach(System.out::println);
	}

}
