package com.classpath.io.orders.exception;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Component
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleInvalidOrder(IllegalArgumentException exception) {
		log.error("Handle exception");
		return new Error(100, exception.getMessage());
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Error handleInvalidInput(MethodArgumentNotValidException exception) {
		
		Set<String> errorMessages = exception
										.getAllErrors()
										.stream()
										.map(err -> err.getDefaultMessage())
										.collect(Collectors.toSet());
		return new Error(400, errorMessages);
	}
}

@AllArgsConstructor
@Getter
class Error {
	private int code;
	private Object message;
}