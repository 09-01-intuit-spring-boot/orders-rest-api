package com.classpath.io.orders.config;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.classpath.io.orders.model.LineItem;
import com.classpath.io.orders.model.Order;
import com.classpath.io.orders.repository.OrderJpaRepository;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
@Profile({"dev", "qa"})
public class BootstrapAppData {
	
	private final OrderJpaRepository repository;
	
	private final Faker faker;
	
	@Value("${app.orderCount}")
	private int count;
	
	@EventListener(ApplicationReadyEvent.class)
	public void handleApplicationReadyEvent(ApplicationReadyEvent event) {
		//insert data
		
		IntStream.range(0, count).forEach(index  -> {
			//@Builder and Faker
			/*
			 * Person p = new Person("Anand", "Ram", 16, true, false, 23);
			 */
			Name name = faker.name();
			Order order = Order.builder()
								.customerName(name.firstName())
								.customerEmail(name.firstName()+"@"+faker.internet().domainName())
								.orderDate(faker.date().past(7, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
								.build();
			
			IntStream.range(0, 3).forEach(value -> {
				LineItem lineItem = LineItem.builder()
										.name(faker.commerce().productName())
										.qty(faker.number().numberBetween(2, 6))
										.price(faker.number().randomDouble(2, 400, 600))
										.build();
				double totalOrderPrice = order.getPrice() + lineItem.getQty() * lineItem.getPrice();
				order.setPrice(totalOrderPrice);
				order.addLineItem(lineItem);
			});
			this.repository.save(order);
		});
	}
}
