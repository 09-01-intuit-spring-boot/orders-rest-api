package com.classpath.io.orders.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

import com.classpath.io.orders.repository.OrderJpaRepository;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator {

	private final OrderJpaRepository repository;
	
	@Override
	public Health health() {
		long count = this.repository.count();

		return count == 0 ? Health.down().withDetail("DB is down", "DB service is up").build(): Health.down().withDetail("DB is up", "DB service is up").build(); 
	}

}

@Configuration
@RequiredArgsConstructor
class KafkaHealthIndicator implements HealthIndicator {
	@Override
	public Health health() {
		return Health.up().withDetail("Kafka-service", "Kafka service is up").build(); 
	}
}
