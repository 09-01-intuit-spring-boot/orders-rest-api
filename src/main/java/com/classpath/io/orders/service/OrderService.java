package com.classpath.io.orders.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.classpath.io.orders.model.Order;
import com.classpath.io.orders.model.OrderDto;
import com.classpath.io.orders.repository.OrderJpaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final OrderJpaRepository repository;
	
	public Order saveOrder(Order order) {
		return this.repository.save(order);
	}
	
	public Map<String, Object> fetchOrders(int page, int size, String direction, String field){
		Sort.Direction sort = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC: Sort.Direction.DESC;
		PageRequest pageRequest = PageRequest.of(page, size, sort, field);
		Page<Order> pageResponse = this.repository.findAll(pageRequest);
		
		long totalRecords = pageResponse.getTotalElements();
		int recordsPerPage = pageResponse.getSize();
		int pages = pageResponse.getTotalPages();
		int current = pageResponse.getNumber();
		List<Order> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("total-records", totalRecords);
		response.put("size", recordsPerPage);
		response.put("total-pages", pages);
		response.put("page", current);
		response.put("data", data);
		
		return response;
	}
	
	public Map<String, Object> fetchOrdersByPriceRange(int page, int size, double min, double max){
		
		PageRequest pageRequest = PageRequest.of(page, size);
		Page<OrderDto> pageResponse = this.repository.findByPriceBetween(min, max, pageRequest);
		
		long totalRecords = pageResponse.getTotalElements();
		int recordsPerPage = pageResponse.getSize();
		int pages = pageResponse.getTotalPages();
		int current = pageResponse.getNumber();
		List<OrderDto> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("total-records", totalRecords);
		response.put("size", recordsPerPage);
		response.put("total-pages", pages);
		response.put("page", current);
		response.put("data", data);
		
		return response;
	}
	
	public void deleteOrderById(long id) {
		this.repository.deleteById(id);
	}
	
	public Order fetchOrderById(long id) {
		return this.repository.findById(id).orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}
	
	public Order updateOrderById(long id, Order order) {
		return this.repository
				.findById(id)
				.map(returnedOrder -> {
					returnedOrder.setCustomerEmail(order.getCustomerEmail());
					returnedOrder.setCustomerName(order.getCustomerName());
					returnedOrder.setOrderDate(order.getOrderDate());
					returnedOrder.setLineItems(order.getLineItems());
					returnedOrder.setPrice(order.getPrice());
					return returnedOrder;
				}).orElse(order);
	}

}
