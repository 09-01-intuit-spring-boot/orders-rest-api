package com.classpath.io.orders.controller;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.io.orders.model.Order;
import com.classpath.io.orders.service.OrderService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/orders")
public class OrderRestController {
	private final OrderService orderService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody @Valid Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@GetMapping
	public Map<String, Object> fetchOrders(
			@RequestParam(name = "page", defaultValue = "0", required = false) int page, 
			@RequestParam(name = "size", defaultValue = "10", required = false) int size, 
			@RequestParam(name = "sort", defaultValue = "asc", required = false) String direction, 
			@RequestParam(name = "field", defaultValue = "customerName", required = false) String field){
		return this.orderService.fetchOrders(page, size, direction, field);
	}
	
	@GetMapping("/price")
	public Map<String, Object> fetchOrders(
			@RequestParam(name = "page", defaultValue = "0", required = false) int page, 
			@RequestParam(name = "size", defaultValue = "10", required = false) int size, 
			@RequestParam(name = "min", defaultValue = "1000", required = false) double min, 
			@RequestParam(name = "max", defaultValue = "5000", required = false) double max){
		return this.orderService.fetchOrdersByPriceRange(page, size, min, max);
	}
	
	@GetMapping("/{id}")
	public Order findOrderById(@PathVariable("id") long id) {
		return this.orderService.fetchOrderById(id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable("id") long id) {
		this.orderService.deleteOrderById(id);
	}
}