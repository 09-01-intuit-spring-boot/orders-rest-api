package com.classpath.io.orders.controller;

import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/health")
@RequiredArgsConstructor
public class ApplicationHealthRestController {

	private final ApplicationEventPublisher eventPublisher;
	
	private final ApplicationAvailability availability;
	
	@PostMapping("/liveness")
	public void updateLivenessEndpoint() {
		
		ApplicationAvailability availability = this.availability;
		LivenessState livenessState = availability.getLivenessState();
		LivenessState updatedState = livenessState == LivenessState.BROKEN ? LivenessState.CORRECT : LivenessState.BROKEN;
		AvailabilityChangeEvent.publish(this.eventPublisher, "i[dated liveness state", updatedState);

	}

	@PostMapping("/readiness")
	public void updateReadinessEndpoint() {
		ApplicationAvailability availability = this.availability;
		ReadinessState readinessState = availability.getReadinessState();
		ReadinessState updatedState = readinessState == ReadinessState.REFUSING_TRAFFIC ? ReadinessState.ACCEPTING_TRAFFIC: ReadinessState.REFUSING_TRAFFIC;
	}

}
