package com.classpath.io.orders.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpath.io.orders.model.Order;
import com.classpath.io.orders.model.OrderDto;

@Repository
public interface OrderJpaRepository extends JpaRepository<Order, Long>{
	
	Set<Order> findByOrderDateBetween(LocalDate startDate, LocalDate endDate);
	
	Set<Order> findByCustomerName(String name);
	
	Page<OrderDto> findByPriceBetween(double min, double max, Pageable page);
	
	Set<Order> findByPriceGreaterThan(double min);

}
